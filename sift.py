from PCV.localdescriptors.sift import process_image, read_features_from_file, plot_features
from pylab import *
from PIL import Image
from argparse import ArgumentParser
import os


p = ArgumentParser()
p.add_argument("input", help="path to input image")
p.add_argument("--force", default=False, action="store_true")
p.add_argument("--draw", default=False, action="store_true")
args = p.parse_args()

impath = args.input
siftpath = os.path.splitext(impath)[0] + ".sift"
siftpngpath = siftpath+".png"

if args.force or (not (os.path.exists(siftpath) and os.path.exists(siftpngpath))):
	process_image(impath,siftpath)

if args.draw:
	im = array(Image.open(impath).convert('L'))
	l, d = read_features_from_file(siftpath)
	figure()
	gray()
	plot_features(im,l, circle=True)
	#show()
	savefig(siftpngpath)
