import sys
from argparse import ArgumentParser

p = ArgumentParser()
p.add_argument("input")

args = p.parse_args()

from numpy import load

with open(args.input) as f:
	data = load(f)

sys.stdout.write("| - |")
for i in range(data.shape[0]):
	sys.stdout.write(" {0} |".format(i+1))
sys.stdout.write("\n")
sys.stdout.write("|---|")
for i in range(data.shape[0]):
	sys.stdout.write("---:|")
sys.stdout.write("\n")

for i, row in enumerate(data):
	sys.stdout.write("| {0} |".format(i+1))
	for j, item in enumerate(row):
		if i == j:
			sys.stdout.write(" - |")
		else:
			sys.stdout.write("{0:0.0f} |".format(item))
	sys.stdout.write("\n")
