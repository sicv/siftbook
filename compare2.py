from PCV.localdescriptors.sift import process_image, read_features_from_file, plot_features, match, match_twosided
from PCV.localdescriptors.harris import appendimages, plot_matches

from pylab import *
from PIL import Image

from argparse import ArgumentParser
import os


p = ArgumentParser()
p.add_argument("input1", help="path to input image 1 (.sift)")
p.add_argument("input2", help="path to input image 2")
p.add_argument("--draw", default=None)
args = p.parse_args()

# siftpath = os.path.splitext(impath)[0] + ".sift"
# siftpngpath = siftpath+".png"

# if args.force or (not (os.path.exists(siftpath) and os.path.exists(siftpngpath))):
# 	process_image(impath,siftpath)

l1, d1 = read_features_from_file(args.input1)
l2, d2 = read_features_from_file(args.input2)
matches = match_twosided(d1, d2)
# print matches
print sum(matches > 0)

if args.draw:
	im1 = array(Image.open(args.input1.replace(".sift", ".png")).convert("L"))
	im2 = array(Image.open(args.input2.replace(".sift", ".png")).convert("L"))

	figure()
	gray()
	plot_matches(im1, im2, l1, l2, matches, False)
	savefig(args.draw)
