var width = 1280,
    height = 700;

var color = d3.scale.category20();

var force = d3.layout.force()
    .charge(-10)
    .linkDistance(75)
    .gravity(0.0)
    .size([width, height]);

var svg = d3.select("#chart").append("svg")
    .attr("width", width)
    .attr("height", height);

var img = d3.select("body")
  .append("div")
  .attr("class", "image")
  .append("img");

function show (im) {
  img.attr("src", im);
}

d3.json("graph.json", function(json) {
  // console.log("data", json);
  force
      .nodes(json.nodes)
      .links(json.links)
      .start();

  var link = svg.selectAll("line.link")
      .data(json.links)
    .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", 1);

  var node = svg.selectAll("g.node")
      .data(json.nodes)
    .enter().append("g")
      .attr("class", "node")
      .attr("xlink:href", function (d) { return d.thumb })
      .attr("r", 5)
      .style("fill", function(d) { return color(0); })
      .call(force.drag);
  node
    .append("image")
    .attr("xlink:href", function (d) { return d.thumb.path })
    .attr("width", function (d) { return d.thumb.width })
    .attr("height", function (d) { return d.thumb.height })
    .attr("x", -75/2)
    .attr("y", -100/2)
    .on("click", function (d) {
      show(d.path);
    });

  node.append("title")
      .text(function(d) { return d; });

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("transform", function(d) { return "translate("+d.x+","+d.y+")"; });
  });
});
