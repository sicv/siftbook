from argparse import ArgumentParser
from PCV.localdescriptors.sift import read_features_from_file, match_twosided
from pylab import *


p = ArgumentParser()
p.add_argument("input", nargs="*")
p.add_argument("--output", default="matchscores.pkl")
args = p.parse_args()
nargs = len(args.input)

matchscores = zeros((nargs,nargs))

for i in range(nargs):
	for j in range(i+1, nargs):
		print 'comparing ', i, j
		print args.input[i]
		l1,d1 = read_features_from_file(args.input[i])
		print args.input[j]
		l2,d2 = read_features_from_file(args.input[j])
		matches = match_twosided(d1, d2)
		nbr_matches = sum(matches > 0)
		matchscores[i, j] = nbr_matches
		matchscores[j, i] = nbr_matches

matchscores.dump(args.output)
