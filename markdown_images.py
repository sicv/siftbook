import sys, os	
from argparse import ArgumentParser

p = ArgumentParser()
p.add_argument("input", nargs="*")
p.add_argument("--ext", default="")
args = p.parse_args()

while True:
	line = sys.stdin.readline()
	if line == '':
		break
	ipath = line.rstrip()
	if ipath:
		path, ext = os.path.splitext(ipath)
		base = os.path.basename(ipath)
		if args.ext:
			dpath, filename = os.path.split(ipath)
			filename, ext = os.path.splitext(filename)
			use_path = os.path.join(dpath, filename + args.ext)
		else:
			use_path = ipath
		print "![{0}]({1})".format(base, use_path)