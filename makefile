source = arkiv0002.png arkiv0004.png arkiv0006.png arkiv0016.png arkiv0027.png arkiv0028.png arkiv0038.png arkiv0042.png arkiv0156.png arkiv0206.png arkiv0285.png $(shell ls *.jpg)
norms = $(patsubst %.jpg,%.640x.png,$(patsubst %.png,%.640x.png,$(source)))
thumbs = $(patsubst %.jpg,%.thumb.png,$(patsubst %.png,%.thumb.png,$(source)))
icons = $(patsubst %.jpg,%.icon.png,$(patsubst %.png,%.icon.png,$(source)))
sift = $(norms:%.png=%.sift)

print-%:
	@echo '$*=$($*)'

all: $(source) $(norms) $(sift) $(thumbs) $(icons) matchscores.pkl

arkiv%.pdf: ../ARKIV-hele.pdf
	pdftk $< cat $* output $@

%.png: %.pdf
	convert $< $@

%.640x.png: %.png
	convert -resize 640x $< $@

%.640x.png: %.jpg
	convert -resize 640x $< $@

%.thumb.png: %.png
	convert -resize 128x $< $@

%.thumb.png: %.jpg
	convert -resize 128x $< $@

%.icon.png: %.png
	convert -resize 32x $< $@

%.icon.png: %.jpg
	convert -resize 32x $< $@

%.sift: %.png
	python sift.py --draw $<
	mogrify -trim $<.png

%.sift: %.jpg
	python sift.py --draw $<

%.html: %.md styles.css
	pandoc --css styles.css --from markdown --to html --standalone $< -o $@

matchscores.pkl: $(sift)
	python compare.py --output $@ $(sift)

graph.json: matchscores.pkl
	python graph.py --matchscores matchscores.pkl $(norms)